#ifndef CLOPTIONS_H
#define CLOPTIONS_H

#include <getopt.h>

namespace
{

    struct config_type
    {
        std::string amqp_host{"localhost"};
        int         amqp_port{5672};
        std::string amqp_exchange{"test_exchange"};
        std::string amqp_queue{"test_queue"};
        std::string amqp_rkey{"test_rkey"};
        std::string db_connect;
    };
//X(prefetch_count,required_argument, nullptr, 'f', "RabbitMQ unacknowledged messages limit")
    #define OPTS_LIST \
    X(help,          no_argument,       nullptr, 'h', "This help") \
    X(amqp_host,     required_argument, nullptr, 'm', "AMQP server address") \
    X(amqp_port,     required_argument, nullptr, 'p', "AMQP server port") \
    X(amqp_exchange, required_argument, nullptr, 'e', "AMQP exchange name") \
    X(amqp_queue,    required_argument, nullptr, 'q', "AMQP input queue name") \
    X(amqp_rkey,     required_argument, nullptr, 'k', "AMQP input routing key name") \
    X(db_connect,    required_argument, nullptr, 'c', "DB connect in Postgres style: \
DRIVER=<path-to-psqlodbca.so>;DATABASE=<DB>;UID=<DB-user-name>;PWD=<password>;\
SERVER=<DB-server-address>;PORT=<port>;")

    const char* short_opts = "hm:p:e:q:k:c:";

    const option opts[] = {
    #define X(name, arg_svr, flag, short_opt, desc) { #name, arg_svr, flag, short_opt },
        OPTS_LIST
    #undef X
        {0, 0, 0, 0}
    };

    void print_help()
    {
        std::cout << "Usage: program <options>"
                << std::endl << "Options:" << std::endl;
    #define X(name, arg_svr, flag, short_name, desc) \
    {\
        char short_opt[2] = {short_name};\
    std::cout << " -" << short_opt << ", --" << #name \
        << std::endl << "  " << "necessity: " << arg_svr \
        << std::endl << "  " << "description: " << desc << std::endl;\
    }
        OPTS_LIST
    #undef X
    }

    bool validate_config(config_type& config)
    {
        return !(config.amqp_exchange.empty() || config.amqp_host.empty()
                || config.amqp_queue.empty() || config.amqp_rkey.empty() 
                || config.db_connect.empty() || (config.amqp_port == -1));
    }

    bool get_config(int argc, char** argv, config_type& config)
    {
        int res;
        int ind;
        while((res = getopt_long(argc, argv, short_opts, opts, &ind)) != -1)
        {
            switch(res)
            {
                case 'h':
                    print_help();
                break;
                case 'm':
                    config.amqp_host = optarg;
                break;
                case 'p':
                {
                    std::string port_str{optarg};
                    try
                    {
                        config.amqp_port = std::stoi(port_str);
                    }
                    catch(std::exception &)
                    {
                        std::cerr << "Could not read port value. "
                                << "Got input: '" << optarg << "'" << std::endl;
                        return false;
                    }
                    break;
                }
                case 'e':
                {
                    config.amqp_exchange = optarg;
                    if(config.amqp_exchange.empty())
                    {
                        
                    }
                    break;
                }
                    
                case 'q':
                    config.amqp_queue = optarg;
                break;
                case 'k':
                    config.amqp_rkey = optarg;
                break;
                case 'c':
                    config.db_connect = optarg;
                break;
                default:
                {
                    std::cerr << "Invalid option: " << char(res);
                    return false;
                }
            }
        }
        return validate_config(config);
    }
}

#endif /* CLOPTIONS_H */


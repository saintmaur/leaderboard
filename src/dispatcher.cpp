#include "dispatcher.h"
#include <iostream>
#include <sys/poll.h>

dispatcher::dispatcher(size_t bulk_size, size_t timeout,
        user_inserter& ui, user_updater& uu, user_deal_inserter& udi)
// input    
    : user_reg_bulks_(bulk_size, 1000)
    , user_upd_bulks_(bulk_size, 1000)
    , user_deal_bulks_(bulk_size, 1000)
// output
    , user_inserter_queue_(ui.get_queue())
    , user_updater_queue_(uu.get_queue())
    , user_deal_inserter_queue_(udi.get_queue())
// general
    , timeout_(timeout)
    , last_time_check_(msg::time_point::clock::now())
    
{}

void dispatcher::loop()
{
    enum poll_ind : unsigned int
    {
        poll_user_reg,
        poll_user_upd,
        poll_user_deal,
        poll_ind_max
    };
    
    const int BADFD             = -1;
    const int POLL_TIMEOUT_MS   = 250;
    
    while(!stop_requested())
    {
        /*
         * create the array of events queues 
         * with uninitialized structs to prevent false alerts
         */
        std::array<::pollfd, poll_ind::poll_ind_max> pfds({BADFD, 0, 0});

        pfds[poll_ind::poll_user_reg]  = {user_reg_q_.sfd(), POLLIN, 0};
        pfds[poll_ind::poll_user_upd]  = {user_upd_q_.sfd(), POLLIN, 0};
        pfds[poll_ind::poll_user_deal] = {user_deal_q_.sfd(), POLLIN, 0};

        assert(!(pfds[poll_ind::poll_user_reg].revents & POLLIN));
        assert(!(pfds[poll_ind::poll_user_upd].revents & POLLIN));
        assert(!(pfds[poll_ind::poll_user_deal].revents & POLLIN));

        int rc = ::poll(pfds.data(), pfds.size(), POLL_TIMEOUT_MS);
        if (rc > 0)
        {
            if(pfds[poll_ind::poll_user_reg].revents & POLLIN)
            {
                msg::user_reg temp_obj;
                user_reg_q_.pop(temp_obj);
                user_reg_bulks_.push(std::move(temp_obj));
            }
            if(pfds[poll_ind::poll_user_upd].revents & POLLIN)
            {
                msg::user_upd temp_obj;
                user_upd_q_.pop(temp_obj);
                user_upd_bulks_.push(std::move(temp_obj));
            }
            if(pfds[poll_ind::poll_user_deal].revents & POLLIN)
            {
                msg::user_deal temp_obj;
                user_deal_q_.pop(temp_obj);
                user_deal_bulks_.push(std::move(temp_obj));
            }
        }
        else if (rc < 0)
        {
            std::cerr << get_name() << ": Polling error: " << errno << std::endl;
        }
        
        if(check_bulks_ready() || timed_out())
        {
            post_bulks();
            last_time_check_ = current_time();
        }
    }
}

bool dispatcher::check_bulks_ready()
{
    return user_reg_bulks_.get_ready_size()
            || user_upd_bulks_.get_ready_size()
            || user_deal_bulks_.get_ready_size();
}

void dispatcher::post_bulks()
{
    post(user_reg_bulks_, user_inserter_queue_);
    post(user_upd_bulks_, user_updater_queue_);
    post(user_deal_bulks_, user_deal_inserter_queue_);
}        
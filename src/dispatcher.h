#ifndef DISPATCHER_H
#define DISPATCHER_H
#include "active_object.h"
#include "message.h"
#include "event_queue.h"
#include "bulks.h"
#include "db/db_ctrl.h"
#include <chrono>

class dispatcher : public active_object
{
public:
    using timeout_type = std::chrono::milliseconds;
// event queues types aliases
    using user_reg_queue_type = event_queue<msg::user_reg>;
    using user_upd_queue_type = event_queue<msg::user_upd>;
    using user_deal_queue_type = event_queue<msg::user_deal>;
// general methods
    dispatcher(size_t, size_t, user_inserter&, user_updater&, user_deal_inserter&);
    void run()
    {
        last_time_check_ = msg::clock::now();
        run_thread("Dispatcher", &dispatcher::loop, this);
        std::cout << get_name() << " has started" << std::endl;
    }
// event queues getters
    user_reg_queue_type&    get_user_reg_queue()  { return user_reg_q_; }
    user_upd_queue_type&    get_user_upd_queue()  { return user_upd_q_; }
    user_deal_queue_type&   get_user_deal_queue() { return user_deal_q_; }
private:
    void loop();
    msg::time_point current_time() { return msg::clock::now(); }
    bool timed_out()
    {
        return current_time() - last_time_check_ > timeout_;
    }
    bool check_bulks_ready();
    void post_bulks();
    template <typename in_bulks_holder_type, typename out_bulks_holder_type>
    void post(in_bulks_holder_type& bulks_in, out_bulks_holder_type& bulks_out)
    {
        if(bulks_in.check_bulk_ready())
        {
            typename out_bulks_holder_type::value_type bulk;
            bulks_in.get_ready_bulk(bulk);
            bulks_out.push(std::move(bulk));
        }
    }
// event queues declarations
    user_reg_queue_type     user_reg_q_;
    user_upd_queue_type     user_upd_q_;
    user_deal_queue_type    user_deal_q_;
// bulks declarations
    bulks<msg::user_reg>    user_reg_bulks_;
    bulks<msg::user_upd>    user_upd_bulks_;
    bulks<msg::user_deal>   user_deal_bulks_;
// DB controllers queues declarations
    user_inserter::event_queue_type&        user_inserter_queue_;
    user_updater::event_queue_type&         user_updater_queue_;
    user_deal_inserter::event_queue_type&   user_deal_inserter_queue_;

    std::mutex mtx_;
    timeout_type timeout_;
    msg::time_point last_time_check_;
};

#endif /* DISPATCHER_H */


#include "message.h"

namespace msg
{
    const char* user_reg_msg_id         = "user_registered";
    const char* user_upd_msg_id         = "user_renamed";
    const char* user_deal_msg_id        = "user_deal";
    const char* user_deal_won_msg_id    = "user_deal_won";
    const char* user_conn_msg_id        = "user_connected";
    const char* user_disconn_msg_id     = "user_disconnected";
    
    const char* format_str              = "%Y.%m.%d %H:%M:%S";
    
    tm get_time_from_string(const char* format, std::string& date_str, bool& res)
    {
        res = true;
        std::stringstream strm{date_str};
        tm t;
        strm >> std::get_time(&t, format);
        if(strm.fail())
        {
            res = false;
        }
        return t;
    }
    
    user_deal make_user_deal_msg(std::string& msg, std::string& msg_id, msg::ack_type fn)
    {
        auto lbr_pos = msg.find("(");
        auto rbr_pos = msg.find(")");
        size_t pos;
        int user_id = std::stoi(msg.substr(lbr_pos+1), &pos);
        auto comma_pos = msg.find(",");
        tm time;
        if(comma_pos != msg.npos)
        {
            auto st_pos = lbr_pos+pos+2;
            auto time_str = msg.substr(st_pos, comma_pos-st_pos);
            bool res;
            time = get_time_from_string(format_str, time_str, res);
        }
        else
        {
            std::cerr 
                    << "Invalid params count for user_deal message" 
                    << std::endl;
            throw std::invalid_argument("Invalid params count for user_deal message");
        }
        comma_pos = msg.find(",", comma_pos+1);
        auto amount_str = msg.substr(comma_pos+1, rbr_pos-comma_pos);
        auto amount = std::stod(amount_str);

        int win_flag = 0;
        if(msg_id.compare(user_deal_won_msg_id) == 0)
        {
            win_flag = 1;
        }
        return user_deal{user_id, amount, time, win_flag, fn};
    }
    
    user_upd make_user_upd_msg(std::string& msg, std::string& msg_id, msg::ack_type fn)
    {
        auto lbr_pos = msg.find("(");
        auto rbr_pos = msg.find(")");
        size_t pos;
        int user_id = std::stoi(msg.substr(lbr_pos+1), &pos);
        std::string name;
        int conn_state = 0;
        if(msg_id.compare(user_upd_msg_id) == 0)
        {
            if(rbr_pos != msg.npos)
            {
                auto st_pos = lbr_pos+pos+2;
                name = msg.substr(st_pos, rbr_pos-st_pos);
            }
        }
        if(msg_id.compare(user_conn_msg_id) == 0)
        {
            conn_state = 1;
        }
        return user_upd{user_id, name, conn_state, fn};
    }
    
    user_reg make_user_reg_msg(std::string& msg, std::string& msg_id, msg::ack_type fn)
    {
        auto lbr_pos = msg.find("(");
        auto rbr_pos = msg.find(")");
        size_t pos;
        int user_id = std::stoi(msg.substr(lbr_pos+1), &pos);

        std::string name;
        if(rbr_pos != msg.npos)
        {
            auto st_pos = lbr_pos+pos+2;
            name = msg.substr(st_pos, rbr_pos-st_pos);
        }
        return user_reg{user_id, name, fn};
    }
}
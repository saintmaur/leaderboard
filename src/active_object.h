#ifndef ACTIVE_OBJECT_H
#define ACTIVE_OBJECT_H

#include <cassert>
#include <thread>
#include <atomic>
#include <utility>
#include <string>

class active_object
{
public:
    active_object(const active_object&) = delete;
    active_object(active_object&&) = delete;
    active_object& operator=(const active_object&) = delete;
    active_object& operator=(active_object&&) = delete;

    active_object() = default;
    virtual ~active_object()
    {
        assert(!thread_.joinable());
        assert(stop_);
    }

    void stop() noexcept
    {
        stop_ = true;
    }

    void complete() noexcept
    {
        complete_ = true;
    }

    bool is_finished() noexcept
    {
        return finished_;
    }

    void join()
    {
        assert(stop_ || complete_);
        assert(thread_.joinable());
        thread_.join();
    }
    bool is_started() {return started_.load();}
protected:
    using atomic_flag_type = std::atomic_bool;
   
    template <typename Callable, typename ...Args>
    void run_thread(const char * name, Callable callable, Args&&... args)
    {
        assert(!thread_.joinable());
        assert(!stop_);
        std::thread t(callable, std::forward<Args>(args)...);
        thread_.swap(t);
        set_name(name);
        set_started();
    }

    bool stop_requested() const         {return stop_;}
    void set_finished() noexcept        {finished_ = true;}
    void set_started() noexcept         {started_ = true;}
    void set_name(const char* name)     {name_ = name;}
    const std::string& get_name() const {return name_;}

private:
    atomic_flag_type stop_{false};
    atomic_flag_type started_{false};
    atomic_flag_type complete_{false};
    atomic_flag_type finished_{false};
    std::thread thread_;
    std::string name_;
    
};

#endif    /* ACTIVE_OBJECT_H */
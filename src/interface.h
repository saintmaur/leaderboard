#ifndef INTERFACE_H
#define INTERFACE_H

#include <amqp_tcp_socket.h>
#include <amqp.h>
#include <amqp_framing.h>
#include "active_object.h"
#include "message.h"
#include "event_queue.h"
#include "dispatcher.h"
#include <sys/poll.h>

class dispatcher;

class amqp_interface : public active_object
{
protected:
    std::string             host_;
    int                     port_;
    amqp_connection_state_t conn_;
    amqp_channel_t          channel_num_;
    amqp_rpc_reply_t        state_;
    msg::time_point         last_check_time_;
    std::string             exchange_;
    std::string             rkey_;
    std::string             queue_;
    event_queue<msg::user_reg>& user_reg_q_;
    event_queue<msg::user_upd>& user_upd_q_;
    event_queue<msg::user_deal>& user_deal_q_;
public:
    amqp_interface(dispatcher& d, const char* exch, const char* rk, 
            const char* q, const char* host = "localhost", 
            int port = 5672, amqp_channel_t channel_num = 1);
    bool init();
    bool conn_check_timed_out();
    void deinit();
    virtual bool run() = 0;
    void listen_loop();
    bool listen(std::string &msg, uint64_t& dtag);
    amqp_rpc_reply_t    get_state() { return state_; }
    bool connected()
    {
        return conn_ != nullptr;
    }
    amqp_channel_t      get_channel() { return channel_num_; }
    bool declare_exchange();
    bool bind_queue(const char* queue, const char* exchange, const char* rkey);
    bool declare_queue(const char* queue);
    bool consume();
    bool post_message(const char* msg = "");
    bool is_normal();
    std::string get_error_message();
    bool queue_purge(const char* queue);
    bool ack(uint64_t dtag);
};

class amqp_reader : public amqp_interface
{
public:
    amqp_reader(dispatcher& d, const char* exch, const char* rk, 
                const char* q, const char* host = "localhost", 
                int port = 5672, amqp_channel_t channel_num = 1)
            : amqp_interface(d, exch, rk, q, host, port, channel_num)
    {}
    bool run()
    {
        run_thread("RabbitMQ read interface", &amqp_reader::loop, this);
        std::cout << get_name() << " has started" << std::endl;
        return true;
    }
    void loop()
    {
        listen_loop();
    }
};

class amqp_writer : public amqp_interface
{
    using queue_type = event_queue<msg::rating_data>;
    queue_type queue_;
public:
    amqp_writer(dispatcher& d, const char* exch, const char* rk, 
                const char* q, const char* host = "localhost", 
                int port = 5672, amqp_channel_t channel_num = 1)
            : amqp_interface(d, exch, rk, q, host, port, channel_num)
    {}
    bool run()
    {
        run_thread("RabbitMQ write interface", &amqp_writer::loop, this);
        std::cout << get_name() << " has started" << std::endl;
    }
    queue_type& get_queue() { return queue_; };
    void loop()
    {
        post_loop();
    }
    std::string prepare_message(msg::rating_data& rd);
    void post_loop();
};

#endif /* INTERFACE_H */


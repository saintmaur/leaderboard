/* Copyright (C) 1991-2016 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */


/* This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it.  */

/* glibc's intent is to support the IEC 559 math functionality, real
   and complex.  If the GCC (4.9 and later) predefined macros
   specifying compiler intent are available, use them to determine
   whether the overall intent is to support these features; otherwise,
   presume an older compiler has intent to support these features and
   define these macros by default.  */



/* wchar_t uses Unicode 8.0.0.  Version 8.0 of the Unicode Standard is
   synchronized with ISO/IEC 10646:2014, plus Amendment 1 (published
   2015-05-15).  */

/* We do not support C11 <threads.h>.  */

//#include "dispatcher.h"
//#include "./db/db_ctrl.h"
//#include "interface.h"
//#include <csignal>
//#include <iostream>
//
//#include "message.h"
//#include <getopt.h>
namespace
{
    bool signaled = false;
}

void signal_handler(int sig)
{
    std::cout << "Got a signal: " << sig 
            << ". Will stop the threads now." << std::endl;
    signaled = true;
}

struct config_type
{
    std::string amqp_host;
    int         amqp_port;
    std::string amqp_exchange;
    std::string amqp_queue;
    std::string amqp_rkey;
    std::string db_connect;
};


const char* short_opts = "hm:p:e:q:k:c:";

const option opts[] = {
    { #help,           no_argument,        nullptr, "'## h'" }, { #amqp_host,      required_argument,  nullptr, "'## m'" }, { #amqp_port,      required_argument,  nullptr, "'## p'" }, { #amqp_exchange,  required_argument,  nullptr, "'## e'" }, { #amqp_queue,     required_argument,  nullptr, "'## q'" }, { #amqp_rkey,      required_argument,  nullptr, "'## k'" }, { #db_connect,     required_argument,  nullptr, "'## c'" },
    {0, 0, 0, 0}
};

void print_help()
{
    std::cout << "Usage: program <options>"
            << std::endl << "Options:" << std::endl;
    std::cout << " -" << ""# h"" << ", --" << #help     << std::endl << "  " << "necessity: " <<           no_argument     << std::endl << "  " << "description: " <<  "This help" << std::endl; std::cout << " -" << ""# m"" << ", --" << #amqp_host     << std::endl << "  " << "necessity: " <<      required_argument     << std::endl << "  " << "description: " <<  "AMQP server address" << std::endl; std::cout << " -" << ""# p"" << ", --" << #amqp_port     << std::endl << "  " << "necessity: " <<      required_argument     << std::endl << "  " << "description: " <<  "AMQP server port" << std::endl; std::cout << " -" << ""# e"" << ", --" << #amqp_exchange     << std::endl << "  " << "necessity: " <<  required_argument     << std::endl << "  " << "description: " <<  "AMQP exchange name" << std::endl; std::cout << " -" << ""# q"" << ", --" << #amqp_queue     << std::endl << "  " << "necessity: " <<     required_argument     << std::endl << "  " << "description: " <<  "AMQP input queue name" << std::endl; std::cout << " -" << ""# k"" << ", --" << #amqp_rkey     << std::endl << "  " << "necessity: " <<      required_argument     << std::endl << "  " << "description: " <<  "AMQP input routing key name" << std::endl; std::cout << " -" << ""# c"" << ", --" << #db_connect     << std::endl << "  " << "necessity: " <<     required_argument     << std::endl << "  " << "description: " <<  "DB connect in Postgres style:DRIVER=<path-to-psqlodbca.so>;DATABASE=<DB>;UID=<DB-user-name>;PWD=<password>;SERVER=<DB-server-address>;PORT=<port>;" << std::endl;
}

bool get_config(int argc, char** argv, config_type& config)
{
    int res;
    int ind;
    while(! (res = getopt_long(argc, argv, short_opts, opts, &ind)) == -1)
    {
        switch(res)
        {
            case 'h':
                print_help();
            break;
            case 'm':
                config.amqp_host = optarg;
            break;
            case 'p':
            {
                std::string port_str{optarg};
                try
                {
                    config.amqp_port = std::stoi(port_str);
                }
                catch(std::exception &)
                {
                    std::cerr << "Could not read port value. "
                            << "Got input: '" << optarg << "'" << std::endl;
                    return false;
                }
                break;
            }
            case 'e':
                config.amqp_exchange = optarg;
            break;
            case 'q':
                config.amqp_queue = optarg;
            break;
            case 'k':
                config.amqp_rkey = optarg;
            break;
            case 'c':
                config.db_connect = optarg;
            break;
            default:
            {
                std::cerr << "Invalid option: " << res;
                return false;
            }
        }
    }
    return true;
}

int main(int argc, char** argv)
{
    if(argc < 7)
    {
        print_help();
        return 1;
    }
    config_type config;
    if(!get_config(argc, argv, config))
    {
        return 1;
    }
    bool start_ok = true;
    const unsigned bulk_size = 1000;
    signal(SIGINT, signal_handler);

    user_inserter       user_reg_ctrl("user_reg", config.db_connect, 
                                        std::chrono::seconds(1), bulk_size);
    user_updater        user_upd_ctrl("user_upd", config.db_connect, 
                                        std::chrono::seconds(1), bulk_size);
    user_deal_inserter  user_deal_ctrl("user_deal", config.db_connect, 
                                        std::chrono::seconds(1), bulk_size);
    dispatcher d(bulk_size, 500, user_reg_ctrl, user_upd_ctrl, user_deal_ctrl);
    
    amqp_reader         amqpreader(d, config.amqp_exchange.c_str(), 
                                    config.amqp_queue.c_str(), 
                                    config.amqp_rkey.c_str(), 
                                    config.amqp_host.c_str(), 
                                    config.amqp_port, 1);
    amqp_writer         amqpwriter(d, config.amqp_exchange.c_str(), 
                                    config.amqp_queue.c_str(), 
                                    config.amqp_rkey.c_str(), 
                                    config.amqp_host.c_str(), 
                                    config.amqp_port, 2);
    rating_reader       reader("rating_data", config.db_connect, 
                                std::chrono::seconds(60), amqpwriter.get_queue());
    
    
    
    try
    {
        start_ok = start_ok && user_reg_ctrl.run();
        start_ok = start_ok && user_upd_ctrl.run();
        start_ok = start_ok && user_deal_ctrl.run();
        
        if(start_ok)
        {
            d.run();
        }
    }
    catch(std::exception &e)
    {
        std::cerr << "A standard exception has been caught: " 
                                    << e.what() << std::endl;
        start_ok = false;
    }
    catch(...)
    {
        std::cerr << "An unknown exception has been caught";
        start_ok = false;
    }
    if(start_ok)
    {
        while(!signaled){}
    }
    
    d.stop();
    user_reg_ctrl.stop();
    user_upd_ctrl.stop();
    user_deal_ctrl.stop();
    
    if(d.is_started())
    {
        d.join();
    }
    
    if(user_deal_ctrl.is_started())
    {
        user_deal_ctrl.join();
    }
    if(user_upd_ctrl.is_started())    
    {
        user_upd_ctrl.join();
    }
    if(user_reg_ctrl.is_started())
    {
        user_reg_ctrl.join();
    }
    
    std::cout << "All the threads have been stopped." << std::endl;
    
    return 0;
}


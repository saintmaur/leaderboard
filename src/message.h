#ifndef MESSAGE_H
#define MESSAGE_H
#include <chrono>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <time.h>
#include <vector>
#include <functional>

namespace msg
{
    extern const char* user_reg_msg_id;
    extern const char* user_upd_msg_id;
    extern const char* user_deal_msg_id;
    extern const char* user_deal_won_msg_id;
    extern const char* user_conn_msg_id;
    extern const char* user_disconn_msg_id;
    
    tm get_time_from_string(const char* format, std::string& date_str, bool& res);

    using clock = std::chrono::high_resolution_clock;
    using time_point = std::chrono::time_point<clock>;

    class sequence
    {
    public:
        static sequence& inst()
        {
            static sequence seq(10000);
            return seq;
        }
        int get_id()
        {
            return id++;
        }
    private:
        sequence(int i):id(i){};
        int id;
    };
    
    struct msg
    {
        using ack_type = std::function<void(void)>;
        msg() : ack([](){}) {}
        msg(ack_type fn) : ack(fn) {}
        void set_ack(ack_type fn) { ack = fn; }
        void basic_ack() { ack(); }
    protected:
        ack_type ack;
    };
    
    struct user_reg : public msg
    {
        int         id;
        std::string name;
        user_reg() = default;
        user_reg(int i, std::string n,  
                msg::ack_type fn = [](){}) 
                : msg(fn)
                , id(i)
                , name(n)
                {}
    };
    struct user_upd : public msg
    {
        int         id;
        std::string name;
        int         conn_state;
        user_upd() = default;
        user_upd(int i, std::string n, int cs,
                msg::ack_type fn = [](){}) 
                : msg(fn)
                , id(i)
                , name(n)
                , conn_state(cs)
                {}
    };
    struct user_deal : public msg
    {
        int     user_id;
        double  amount;
        tm      time;
        int     won;
        user_deal() = default;
        user_deal(int id, double amnt, tm t, int w,
                msg::ack_type fn = [](){}) 
                : msg(fn)
                , user_id(id)
                , amount(amnt)
                , time(t)
                , won(w)
                {}
    };
    struct rating_data
    {
        struct rating_rec
        {
            int     user_id;
            double  amount;
            int     position{-1};
        };
        int                     user_id;
        int                     position{-1};
        std::vector<rating_rec> neighbors;
        std::vector<rating_rec> rating_list;
    };
    
    user_reg make_user_reg_msg(std::string& msg, std::string& msg_id, msg::ack_type fn = [](){});
    user_upd make_user_upd_msg(std::string& msg, std::string& msg_id, msg::ack_type fn = [](){});
    user_deal make_user_deal_msg(std::string& msg, std::string& msg_id, msg::ack_type fn = [](){});
}

#endif /* MESSAGE_H */


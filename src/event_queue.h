#ifndef EVENT_QUEUE_H
#define EVENT_QUEUE_H

#include <unistd.h>
#include <cerrno>
#include <sys/eventfd.h>
#include <mutex>
#include <queue>
#include <iostream>
template<typename T>
class event_queue
{
public:
    using value_type = T;
    event_queue() : fd_(-1)
    {
        if ((fd_ = ::eventfd(0, EFD_SEMAPHORE | EFD_NONBLOCK)) < 0)
            throw std::runtime_error("failed to create the queue event fd");
    }
    ~event_queue()
    {
        ::close(fd_);
    }

    bool empty() const { return queue_.empty(); }
    int sfd() const { return fd_; }

    bool push(value_type &&req)
    {
        std::lock_guard<std::mutex> lock(mtx_);
        try
        {
            queue_.push(std::move(req));
            sig();
            return true;
        }
        catch(std::exception &e)
        {
            return false;
        }
    }

    bool pop(value_type &req)
    {
        std::lock_guard<std::mutex> lock(mtx_);
        if(!check())
        {
            return false;
        }
        assert(!queue_.empty());
        auto sz = queue_.size();
        req = std::move(queue_.front());
        queue_.pop();
        return true;
    }

private:
    bool check()
    {
        uint64_t s;
        auto r = ::read(fd_, &s, sizeof(s));
        if(r > 0)
        {
            assert(r == 8);
            return true;
        }
        if(!r || errno == EWOULDBLOCK || errno == EAGAIN)
        {
            return false;
        }
        throw std::runtime_error("Failed to check the signal on the queue");
    }
    void sig()
    {
        uint64_t s = 1;
        if (::write(fd_, &s, sizeof(s)) <= 0)
            throw std::runtime_error("failed to signal to queue file descriptor");
    }
    std::queue<value_type> queue_;
    std::mutex mtx_;
    int fd_;
};

#endif /* EVENT_QUEUE_H */


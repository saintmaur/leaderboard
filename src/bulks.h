#ifndef BULKS_H
#define BULKS_H

#include <queue>
#include <mutex>

#include "message.h"

template <typename T>
class bulks
{
public:
    using op_strg_type = std::vector<T>;
    using bulks_strg_type = std::queue<op_strg_type>;
    
    bulks(size_t s, unsigned ms = 0)
            : max_size_(s)
            , timeout_(std::chrono::milliseconds(ms))
    {
        oper_strg_.reserve(s);
        set_time();
    }

    op_strg_type& get_oper_storage() {return oper_strg_;}
    bulks_strg_type& get_ready_storage() {return ready_bulks_;}
    bool push(T&& obj)
    {
        std::lock_guard<std::mutex> l(m_);
        oper_strg_.push_back(std::forward<T>(obj));
        if(oper_strg_.size() >= max_size_ || timed_out())
        {
            op_strg_type temp_strg;
            temp_strg.swap(oper_strg_);
            ready_bulks_.push(std::move(temp_strg));
            set_time();
        }
    }
    
    bool check_bulk_ready()
    {
        std::lock_guard<std::mutex> l(m_);
        bool res = false;
        if(timed_out() && oper_strg_.size() > 0)
        {
            op_strg_type temp_strg;
            temp_strg.swap(oper_strg_);
            ready_bulks_.push(std::move(temp_strg));
            set_time();
            res = true;
        }
        return res;
    }
    
    size_t get_ready_size() { return ready_bulks_.size(); }
    
    void get_ready_bulk(op_strg_type& bulk)
    {
        std::lock_guard<std::mutex> l(m_);
        bulk = std::move(ready_bulks_.front());
        ready_bulks_.pop();
    }
private:
    void set_time() { last_time_check_ = msg::clock::now(); }
    bool timed_out()
    {
        return msg::clock::now() - last_time_check_ > timeout_;
    }
    std::mutex                  m_;
    size_t                      max_size_;
    op_strg_type                oper_strg_;
    bulks_strg_type             ready_bulks_;
    std::chrono::milliseconds   timeout_;
    msg::time_point             last_time_check_;
    
};

#endif /* BULKS_H */


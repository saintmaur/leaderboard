#include "dispatcher.h"
#include "./db/db_ctrl.h"
#include "interface.h"
#include <csignal>
#include <iostream>

#include "message.h"
#include "cloptions.h"

namespace
{
    bool signaled = false;
}

void signal_handler(int sig)
{
    std::cout << "Got a signal: " << sig 
            << ". Will stop the threads now." << std::endl;
    signaled = true;
}

int main(int argc, char** argv)
{
    if(argc < 13)
    {
        print_help();
        return 1;
    }
    config_type config;
    if(!get_config(argc, argv, config))
    {
        return 1;
    }
    bool start_ok = true;
    const unsigned bulk_size = 1000;
    signal(SIGINT, signal_handler);

    user_inserter       user_reg_ctrl("user_reg", config.db_connect, 
                                        std::chrono::seconds(1), bulk_size);
    user_updater        user_upd_ctrl("user_upd", config.db_connect, 
                                        std::chrono::seconds(1), bulk_size);
    user_deal_inserter  user_deal_ctrl("user_deal", config.db_connect, 
                                        std::chrono::seconds(1), bulk_size);
    dispatcher d(bulk_size, 1000, user_reg_ctrl, user_upd_ctrl, user_deal_ctrl);
    
    amqp_reader         amqpreader(d, config.amqp_exchange.c_str(), 
                                    config.amqp_rkey.c_str(), 
                                    config.amqp_queue.c_str(), 
                                    config.amqp_host.c_str(), 
                                    config.amqp_port, 1);
    amqpreader.init();
    amqp_writer         amqpwriter(d, config.amqp_exchange.c_str(), 
                                    config.amqp_rkey.c_str(), 
                                    config.amqp_queue.c_str(), 
                                    config.amqp_host.c_str(), 
                                    config.amqp_port, 2);
    amqpwriter.init();
    rating_reader       reader("rating_data", config.db_connect, 
                                std::chrono::seconds(60), amqpwriter.get_queue());
    try
    {
        start_ok = start_ok && user_reg_ctrl.run();
        start_ok = start_ok && user_upd_ctrl.run();
        start_ok = start_ok && user_deal_ctrl.run();
        
        start_ok = start_ok && amqpreader.run();
        start_ok = start_ok && amqpwriter.run();
        start_ok = start_ok && reader.run();
        
        if(start_ok)
        {
            d.run();
        }
        std::cout << "All the threads have been started" << std::endl;
        std::cout << "Listening the AMQP config: " << config.amqp_host << ":" 
                << config.amqp_port << "/" << config.amqp_queue << "@" 
                << config.amqp_exchange << " by key: " << config.amqp_rkey << std::endl;
        std::cout << "Press Ctrl+C to stop" << std::endl;
    }
    catch(std::exception &e)
    {
        std::cerr << "A standard exception has been caught: " 
                                    << e.what() << std::endl;
        start_ok = false;
    }
    catch(...)
    {
        std::cerr << "An unknown exception has been caught";
        start_ok = false;
    }
    if(start_ok)
    {
        while(!signaled){}
    }
    
    std::cout << "Stopping..." << std::endl;
    
    d.stop();
    user_reg_ctrl.stop();
    user_upd_ctrl.stop();
    user_deal_ctrl.stop();
    
    amqpreader.stop();
    amqpwriter.stop();
    reader.stop();
    
    if(d.is_started())
    {
        d.join();
    }
    
    if(user_deal_ctrl.is_started())
    {
        user_deal_ctrl.join();
    }
    if(user_upd_ctrl.is_started())    
    {
        user_upd_ctrl.join();
    }
    if(user_reg_ctrl.is_started())
    {
        user_reg_ctrl.join();
    }
    if(amqpreader.is_started())
    {
        amqpreader.join();
    }
    if(amqpwriter.is_started())
    {
        amqpwriter.join();
    }
    if(reader.is_started())
    {
        reader.join();
    }
    
    amqpreader.deinit();
    amqpwriter.deinit();
    
    std::cout << "All the threads have been stopped." << std::endl;
    
    return 0;
}


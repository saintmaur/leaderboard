#include "interface.h"

const unsigned conn_check_timeout_ms    = 1000;

amqp_interface::amqp_interface(dispatcher& d, const char* exch, const char* rk, 
            const char* q, const char* host/* = "localhost"*/, 
            int port/* = 5672*/, amqp_channel_t channel_num/* = 1*/)
            : host_(host)
            , port_(port)
            , conn_(nullptr)
            , channel_num_(channel_num ? channel_num : 1)
            , exchange_(exch)
            , rkey_(rk)
            , queue_(q)
            , user_reg_q_(d.get_user_reg_queue())
            , user_upd_q_(d.get_user_upd_queue())
            , user_deal_q_(d.get_user_deal_queue())
{}

bool amqp_interface::init()
{
    assert(!conn_);
    conn_ =  amqp_new_connection();

    timeval timeout{1,0};
    amqp_set_rpc_timeout(conn_, &timeout);
    
    amqp_socket_t *socket = nullptr;
    socket = amqp_tcp_socket_new(conn_);
    assert(socket);
    auto status = amqp_socket_open(socket, host_.c_str(), port_);
    assert(!status);

    state_ = amqp_login(conn_, "/", 0, 131072, 0, AMQP_SASL_METHOD_PLAIN, "guest", "guest");
    amqp_channel_open(conn_, channel_num_);
    state_ = amqp_get_rpc_reply(conn_);
    if(state_.reply_type != AMQP_RESPONSE_NORMAL)
    {
        std::cout << get_name() << ": error on login: " << get_error_message() << std::endl;
    }

    last_check_time_ = msg::clock::now();
    return connected();
}

bool amqp_interface::conn_check_timed_out()
{
    return msg::clock::now() - last_check_time_ 
            > std::chrono::milliseconds(conn_check_timeout_ms);
}
void amqp_interface::deinit()
{
    assert(conn_);
    amqp_get_rpc_reply(conn_);

    amqp_channel_close(conn_, channel_num_, AMQP_REPLY_SUCCESS);
    state_ = amqp_connection_close(conn_, AMQP_REPLY_SUCCESS);
    amqp_destroy_connection(conn_);
    conn_ = nullptr;
}
void amqp_interface::listen_loop()
{
    consume();
    while(!stop_requested())
    {
        if(conn_check_timed_out())
        {
            if(!connected())
            {
                deinit();
                init();
                consume();
            }
        }
        std::string msg;
        uint64_t dtag;
        if(listen(msg, dtag))
        {
            try
            {
                auto lbr_pos = msg.find("(");
                auto rbr_pos = msg.find(")");
                if(lbr_pos != msg.npos)
                {
                    auto msg_id = msg.substr(0, lbr_pos);
                    if(msg_id.compare(msg::user_reg_msg_id) == 0)
                    {
                        std::cout << get_name() 
                                << ": got user_reg message. "
                                << "Will post it to DB controller" << std::endl;
                        user_reg_q_.push(std::move(msg::make_user_reg_msg(msg, msg_id, [dtag, this](){ ack(dtag); })));
                    }
                    else if(msg_id.compare(msg::user_upd_msg_id) == 0
                            || msg_id.compare(msg::user_conn_msg_id) == 0
                            || msg_id.compare(msg::user_disconn_msg_id) == 0)
                    {
                        std::cout << get_name() 
                                << ": got user_upd message. "
                                << "Will post it to DB controller" << std::endl;
                        user_upd_q_.push(std::move(msg::make_user_upd_msg(msg, msg_id, [dtag, this](){ ack(dtag); })));
                    }
                    else if(msg_id.compare(msg::user_deal_msg_id) == 0
                            || msg_id.compare(msg::user_deal_won_msg_id) == 0)
                    {
                        std::cout << get_name() 
                                << ": got user_deal message. "
                                << "Will post it to DB controller" << std::endl;
                        user_deal_q_.push(std::move(msg::make_user_deal_msg(msg, msg_id, [dtag, this](){ ack(dtag); })));
                    }
                    else
                    {
                        std::cout << get_name() 
                                << ": got an unknown message: " 
                                << std::endl << get_name() << ": " << msg
                                << std::endl << get_name() << ": will skip it"
                                << std::endl;
                    }
                    if(!consume())
                    {
                        std::cout << "Failed to consume: " << get_error_message() << std::endl;
                    }
                }
                else
                {
                    std::cout << get_name() 
                            << ": got an unknown message: " 
                            << std::endl << get_name() << ": " << msg
                            << std::endl << get_name() << ": will skip it"
                            << std::endl;
                }
            }
            catch(std::exception &e)
            {
                std::cerr << get_name() << ": a standard exception has been caught: "
                        << e.what() << std::endl;
            }
        }
        else
        {
            //std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }
    }
}
bool amqp_interface::listen(std::string &msg, uint64_t& dtag)
{
    assert(conn_);
    timeval timeout{1,0};
    amqp_rpc_reply_t res;
    amqp_envelope_t envelope;
    
    amqp_maybe_release_buffers(conn_);
    std::cout << "Waiting for messages..." << std::endl;
    state_ = amqp_consume_message(conn_, &envelope, &timeout, 0);
    std::cout << "Got a message or timed out" << std::endl;
    if (state_.reply_type != AMQP_RESPONSE_NORMAL)
    {
        return false;
    }
    std::cout << get_name() << ": got a message!" << std::endl;
    msg = (char*)envelope.message.body.bytes;
    dtag = envelope.delivery_tag;
    amqp_destroy_envelope(&envelope);
    return true;
}
bool amqp_interface::declare_exchange()
{
    assert(conn_);
    amqp_exchange_declare(conn_, channel_num_, amqp_cstring_bytes(exchange_.c_str()), 
                            amqp_cstring_bytes("direct"),
                            0, 1, 0, 0, amqp_empty_table);
    return is_normal();
}

bool amqp_interface::bind_queue(const char* queue, 
                const char* exchange, 
                const char* rkey)
{
    assert(conn_);
    amqp_queue_bind(conn_, channel_num_,
              amqp_cstring_bytes(queue),
              amqp_cstring_bytes(exchange),
              amqp_cstring_bytes(rkey),
              amqp_empty_table);
    return is_normal();
}

bool amqp_interface::declare_queue(const char* queue)
{
    assert(conn_);
    amqp_queue_declare(conn_, channel_num_, 
                        amqp_cstring_bytes(queue), 
                        0, 0, 0, 0, amqp_empty_table);

    return is_normal();
}

bool amqp_interface::consume()
{
    assert(conn_);
    amqp_basic_consume(conn_, channel_num_, amqp_cstring_bytes(queue_.c_str()), 
                        amqp_empty_bytes, 0, 1, 0, amqp_empty_table);
    return is_normal();
}

bool amqp_interface::queue_purge(const char* queue)
{
    assert(conn_);
    amqp_queue_purge(conn_, channel_num_, amqp_cstring_bytes(queue));
    
    return is_normal();
}

bool amqp_interface::ack(uint64_t dtag)
{
    assert(conn_);
    auto res = 0;
    //amqp_basic_ack(conn_, channel_num_, dtag, 0);
    return res == AMQP_STATUS_OK && is_normal();
}

bool amqp_interface::post_message(const char* msg)
{
    assert(conn_);
    amqp_basic_properties_t props;
    props._flags = AMQP_BASIC_CONTENT_TYPE_FLAG | AMQP_BASIC_DELIVERY_MODE_FLAG;
    props.content_type = amqp_cstring_bytes("text/plain");
    props.delivery_mode = 1;
    amqp_basic_publish(conn_, channel_num_,
            amqp_cstring_bytes(exchange_.c_str()), amqp_cstring_bytes(rkey_.c_str()),
            0, 0, &props, amqp_cstring_bytes(msg));
    return is_normal();
}

bool amqp_interface::is_normal()
{
    assert(conn_);
    state_ = amqp_get_rpc_reply(conn_);
    return state_.reply_type == AMQP_RESPONSE_NORMAL;
}

std::string amqp_interface::get_error_message()
{
    std::string msg;
    std::stringstream ss;
    switch(state_.reply_type)
    {
        case AMQP_RESPONSE_SERVER_EXCEPTION:
        {
            msg = "Server error: ";
            switch(state_.reply.id)
            {
                case AMQP_CHANNEL_CLOSE_METHOD:
                    auto t = (amqp_channel_close_t_*)state_.reply.decoded;
                    ss << (char*)t->reply_text.bytes;
                    break;
            }
            msg.append(ss.str());
            break;
        }
        case AMQP_RESPONSE_LIBRARY_EXCEPTION:
        {
            msg = "Library error: ";
            msg.append(amqp_error_string2(state_.library_error));
            break;
        }
    }
    return msg;
}

std::string amqp_writer::prepare_message(msg::rating_data& rd)
{
    std::string str;
    std::stringstream stream;
    stream << "{user_id:" << rd.user_id << ", position:" << rd.position;
    
    stream << ", top: [";
    for(const auto & rec : rd.rating_list)
    {
        stream << "{user_id:" << rec.user_id << ", amount:" << rec.amount;
        stream << ", position:" << rec.position << "},";
    }
    stream.seekp(-1, stream.cur);
    stream << "]";
    
    stream << ", neighbors: [";
    for(const auto & rec : rd.rating_list)
    {
        stream << "{user_id:" << rec.user_id << ", amount:" << rec.amount;
        stream << ", position:" << rec.position << "},";
    }
    stream.seekp(-1, stream.cur);
    stream << "]}";
    
    return stream.str();
}

void amqp_writer::post_loop()
{
    while(!stop_requested())
    {
        if(conn_check_timed_out())
        {
            if(!connected())
            {
                deinit();
                init();
            }
        }
        std::array<::pollfd, 1> pfds({-1, 0, 0});

        pfds[0] = {queue_.sfd(), POLLIN, 0};

        int rc = ::poll(pfds.data(), pfds.size(), 250);
        if (rc > 0)
        {
            if(pfds[0].revents & POLLIN)
            {
                msg::rating_data temp_obj;
                queue_.pop(temp_obj);
                auto msg = prepare_message(temp_obj);
                if(!post_message(msg.c_str()))
                {
                    std::cout << get_name() << ": failed to post the message" << std::endl;
                }
                else
                {
                    std::cout << get_name() << ": the message has been posted" << std::endl;
                }
            }
        }
        else if (rc < 0)
        {
            std::cerr << get_name() << ": polling error: " << errno << std::endl;
        }
    }
}
#include "db_ctrl.h"

namespace
{
    otl_stream& operator<<(otl_stream& s, tm& time)
    {
        auto dt = get_otl_dt(time);
        return s << dt;
    }
}

bool user_inserter::process_bulk(queue_el_type& bulk)
{
    bool rslt = true;
    try
    {
        init_stream(false);
        
        std::cout << "Starting to process the bulk (size: " << bulk.size() << ")" << std::endl;
        
        for(const auto & rec : bulk)
        {
            stream_ << rec.id << rec.name;
        }
        stream_.flush();
        connect_.commit();
        std::cout << "Users insert (" << bulk.size() << ") has been completed" << std::endl;
    }
    catch(otl_exception &e)
    {
        std::cerr << get_name() << ": An OTL exception has been caught: "
                                        << e.msg << std::endl
                                        << e.var_info << std::endl;
        rslt = false;
    }
    catch(std::exception &e)
    {
        std::cerr << get_name() << ": A standard exception has been caught: "
                                    << e.what() << std::endl;
        rslt = false;
    }
    stream_.close();
    ack(bulk);
    return rslt;
}

bool user_updater::process_bulk(queue_el_type& bulk)
{
    bool rslt = true;
    try
    {
        std::cout << "Starting to process the bulk (size: " << bulk.size() << ")" << std::endl;
        
        init_stream(false);
        
        for(auto & rec : bulk)
        {
            if(rec.name.empty())
            {
                stream_ << otl_null();
            }
            else
            {
                stream_ <<  rec.name;
            }
            stream_ << rec.conn_state << rec.id;
        }
        stream_.flush();
        connect_.commit();
        
        std::cout << "Users update (" << bulk.size() << ") has been completed" << std::endl;
    }
    catch(otl_exception &e)
    {
        std::cerr << get_name() << ": An OTL exception has been caught: "
                                        << e.msg << std::endl
                                        << e.var_info << std::endl;
        rslt = false;
    }
    catch(std::exception &e)
    {
        std::cerr << get_name() << ": A standard exception has been caught: "
                                    << e.what() << std::endl;
        rslt = false;
    }
    stream_.close();
    ack(bulk);
    return rslt;
}

bool user_deal_inserter::process_bulk(queue_el_type& bulk)
{
    bool rslt = true;
    try
    {
        std::cout << "Starting to process the user_deals bulk (size: " << bulk.size() << ")" << std::endl;
        
        init_stream(false);
        
        for(auto & rec : bulk)
        {
            stream_ << rec.user_id << rec.amount << rec.time << rec.won;
        }
        stream_.flush();
        connect_.commit();
        
        std::cout << "User deals insert (" << bulk.size() << ") has been completed" << std::endl;
    }
    catch(otl_exception &e)
    {
        std::cerr << get_name() << ": An OTL exception has been caught: "
                                        << e.msg << std::endl
                                        << e.var_info << std::endl;
        rslt = false;
    }
    catch(std::exception &e)
    {
        std::cerr << get_name() << ": A standard exception has been caught: "
                                    << e.what() << std::endl;
        rslt = false;
    }
    stream_.close();
    ack(bulk);
    return rslt;
}

rating_reader::rating_reader(const char* name, std::string conn_str, 
                            std::chrono::seconds to, poster_queue_type& q)
    : db_ctrl<int>(name, conn_str, to, 1)
    , poster_queue_(q)
{
    sql_str_.assign(sql::select_rates);
}

void rating_reader::upd_nbor(nbor_list_type &nlt, rec_type &rec)
{
    auto it = nlt.begin();
    while(it != nlt.end())
    {
        if(!it->push(rec))
        {
            it = nlt.erase(it);
        }
        else
        {
            it++;
        }
    }
}

void rating_reader::flush_nbor(nbor_list_type& nlt)
{
    auto it = nlt.begin();
    while(it != nlt.end())
    {
        it->flush();
        it = nlt.erase(it);
    }
}

bool rating_reader::read_rating(rate_list_type& rl)
{
    bool rslt = true;
    nbor_list_type nbors;
    try
    {
        init_stream(true);
        
        limited_collection<rec_type> 
            top_list(top_count, [](const limited_collection<rec_type>
                                                ::collection_type& collection){});
        unsigned pos{0};
        while(!stream_.eof())
        {
            msg::rating_data                data;
            msg::rating_data::rating_rec    rec;
            rec.position = pos;

            stream_ >> rec.user_id >> rec.amount;
            top_list.push(rec);
            stream_ >> data.user_id;
            upd_nbor(nbors, rec);
            
            int conn_state{0};
            stream_ >> conn_state;
            if(conn_state > 0)
            {
                data.position = pos;
                limited_collection<rec_type> 
                lc(n_count, 
                        [pos, &rl](const limited_collection<rec_type>
                                                ::collection_type& collection)
                        {
                            rl.at(pos).neighbors.assign(collection.begin(), 
                                                            collection.end());
                        });
                nbors.push_back(lc);
                
                data.rating_list.assign(top_list.collection().begin(), 
                                                top_list.collection().end());
                rl.push_back(data);
            }
            pos++;
        }
        flush_nbor(nbors);
    }
    catch(otl_exception &e)
    {
        std::cerr << get_name() << ": An OTL exception has been caught in: "
                << "error code: " << e.code << std::endl
                << e.msg << std::endl
                << e.var_info << std::endl;
        rslt = false;
    }
    catch(std::exception &e)
    {
        std::cerr << get_name() << ": A standard exception has been caught: "
                                    << e.what() << std::endl;
        rslt = false;
    }
    stream_.close();
    return rslt;
}

void rating_reader::reader_loop()
{
    while(!stop_requested())
    {
        if(db_check_timed_out())
        {
            last_check_time_ = msg::clock::now();
            connect();
            rate_list_type rl;
            if(read_rating(rl))
            {
                post(std::move(rl));
            }
        }
    }
}

void rating_reader::post(rate_list_type &&rl)
{
    for(auto & rate : rl)
    {
        poster_queue_.push(std::move(rate));
    }
}
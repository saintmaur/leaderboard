#ifndef DB_CTRL_H
#define DB_CTRL_H

#define OTL_ODBC_UNIX
#define OTL_ODBC_ALTERNATE_RPC
#define OTL_ODBC
#define OTL_STL

#include "otlv4.h"
#include "sql_resources.h"
#include "../active_object.h"
#include "../message.h"
#include "../event_queue.h"
#include "../bulks.h"
#include <sys/poll.h>
#include <queue>
#include <ctime>

namespace
{
    otl_datetime get_otl_dt(tm& time)
    {
        return otl_datetime(time.tm_year+1900, time.tm_mon+1,
                            time.tm_mday,time.tm_hour,
                            time.tm_min, time.tm_sec);
    }
    otl_stream& operator<<(otl_stream& s, tm& time);
    //std::string make_connect_string()
    template <typename T>
    class limited_collection
    {
    public:
        using collection_type   = std::vector<T>;
        using action_type       = std::function<void(const collection_type& 
                                                                    collection)>;
        limited_collection(unsigned max, action_type fn)
        : action_(fn)
        , max_(max)
        , exceeded_(false)
        {
            collection_.reserve(max_);
        };
        bool push(const T& v)
        {
            bool res = false;
            if(collection_.size() < max_)
            {
                collection_.push_back(v);
                res = true;
            }
            else
            {
                if(!exceeded_)
                {
                    action_(collection_);
                    exceeded_ = true;
                }
            }
            return res;
        }
        void flush()
        {
            if(!exceeded_)
            {
                action_(collection_);
                exceeded_ = true;
            }
        }
        const size_t size() const { return collection_.size(); }
        const collection_type& collection() const { return collection_; }
    private:
        collection_type collection_;
        action_type action_;
        unsigned max_;
        bool exceeded_;
    };
}

template <typename T>
class db_ctrl : public active_object
{
public:
    using queue_el_type     = typename bulks<T>::op_strg_type;
    using event_queue_type  = event_queue<queue_el_type>;
    db_ctrl(const char* name, std::string& conn_str, 
            std::chrono::seconds tms, unsigned bulk_size = 0) 
    : type_name_(name)
    , bulk_size_(bulk_size)
    , conn_str_(conn_str)
    , timeout_(tms)
    {
        otl_connect::otl_initialize();
    }
    virtual ~db_ctrl() noexcept {}
    
    event_queue_type& get_queue() {return queue_;}
    void init_stream(bool auto_flush)
    {
        stream_.set_flush(auto_flush);
        stream_.open(bulk_size_, sql_str_.c_str(), connect_);
    }
    bool connect()
    {
        try
        {
            if(!connect_.connected)
            {
                connect_.rlogon(conn_str_.c_str());
            }
        }
        catch(otl_exception &e)
        {
            std::cerr << "An OTL exception has been caught: "
                                            << e.msg << std::endl;
            std::cerr << "Connection string: " << conn_str_ << std::endl;
        }
        return connect_.connected;
    }
    virtual bool run()
    {
        bool rslt = false;
        if(connect())
        {
            std::string nm{"DB controller ("};
            nm.append(type_name_).append(")");
            last_check_time_ = msg::clock::now();
            run_thread(nm.c_str(), &db_ctrl::writer_loop, this);
            rslt = true;
        }
        std::cout << get_name() << " has started" << std::endl;
        return rslt;
    }
    int is_connected() { return connect_.connected; }
    otl_connect& get_connect() { return connect_; }
protected:
    virtual bool process_bulk(queue_el_type& bulk) { return true; };
    bool ack(queue_el_type& bulk)
    {
        for (auto & msg : bulk)
        {
            msg.basic_ack();
        }
    }
    bool send_left_msgs();
    bool db_check_timed_out()
    {
        return msg::clock::now() - last_check_time_ 
                >= timeout_;
    }
    std::string sql_str_;
    otl_stream  stream_;
    otl_connect connect_;
    unsigned    bulk_size_;
    void writer_loop()
    {
        while(!stop_requested())
        {
            if(db_check_timed_out())
            {
                connect();
            }
            std::array<::pollfd, 1> pfds({BADFD, 0, 0});

            pfds[0] = {queue_.sfd(), POLLIN, 0};
            
            int rc = ::poll(pfds.data(), pfds.size(), POLL_TIMEOUT_MS);
            if (rc > 0)
            {
                if(pfds[0].revents & POLLIN)
                {
                    queue_el_type temp_obj(bulk_size_);
                    queue_.pop(temp_obj);
                    std::cout << "A messages bulk has been recieved."
                            " Size: " << temp_obj.size() << std::endl;
                    if(!process_bulk(temp_obj))
                    {
                        std::cerr << "Failed to process the batch" << std::endl;
                    }
                }
            }
            else if (rc < 0)
            {
                std::cerr << get_name() << "Polling error: " << errno << std::endl;
            }
        }
    }
    std::string      type_name_;
    msg::time_point  last_check_time_;
private:
    const int        BADFD             = -1;
    const int        POLL_TIMEOUT_MS   = 250;
    std::chrono::seconds timeout_;
    std::string      conn_str_;
    event_queue_type queue_;
};

class user_inserter : public db_ctrl<msg::user_reg>
{
public:
    using queue_type = event_queue_type;

    user_inserter(const char* name, std::string conn_str, 
                        std::chrono::seconds timeout_ms = std::chrono::seconds(1), 
                        unsigned bulk_size = 1)
    : db_ctrl<msg::user_reg>(name, conn_str, timeout_ms, bulk_size)
    {
        sql_str_.assign(sql::insert_user);
    }
    ~user_inserter() noexcept {}
    bool process_bulk(queue_el_type& bulk);
};

class user_updater : public db_ctrl<msg::user_upd>
{
public:
    using queue_type = event_queue_type;

    user_updater(const char* name, std::string conn_str, 
                        std::chrono::seconds timeout_ms = std::chrono::seconds(1), 
                        unsigned bulk_size = 1)
    : db_ctrl(name, conn_str, timeout_ms, bulk_size)
    {
        sql_str_.assign(sql::update_user);
    }
    ~user_updater() noexcept {}
    bool process_bulk(queue_el_type& bulk);
};

class user_deal_inserter : public db_ctrl<msg::user_deal>
{
public:
    using queue_type = event_queue_type;

    user_deal_inserter(const char* name, std::string conn_str, 
                        std::chrono::seconds timeout_ms = std::chrono::seconds(1), 
                        unsigned bulk_size = 1)
    : db_ctrl<msg::user_deal>(name, conn_str, timeout_ms, bulk_size)
    {
        sql_str_.assign(sql::insert_deal);
    }
    ~user_deal_inserter() noexcept {}
    bool process_bulk(queue_el_type& bulk);
};

class rating_reader : public db_ctrl<int>
{
public:
    using poster_queue_type = event_queue<msg::rating_data>;
    using rec_type          = msg::rating_data::rating_rec;
    using nbor_list_type    = std::vector<limited_collection<rec_type>>;
    using queue_type        = event_queue_type;
    using rate_list_type    = std::vector<msg::rating_data>;
    
    poster_queue_type& poster_queue_;
    const unsigned top_count = 10;
    const unsigned n_count   = 10;
public:
    rating_reader(const char* name, std::string conn_str, 
                    std::chrono::seconds to, poster_queue_type& q);
    
    bool run()
    {
        bool rslt = false;
        if(connect())
        {
            std::string nm{"DB controller ("};
            nm.append(type_name_).append(")");
            run_thread(nm.c_str(), &rating_reader::loop, this);
            
            last_check_time_ = msg::clock::now();
            rslt = true;
        }
        std::cout << get_name() << " has started" << std::endl;
        return rslt;
    }
    void loop()
    {
        reader_loop();
    }
    bool read_rating(rate_list_type& rl);
    void reader_loop();
    void post(rate_list_type &&rl);
    void set_neighbors(rate_list_type& rl, unsigned ncount = 10);
    void upd_nbor(nbor_list_type &nlt, rec_type &rec);
    void flush_nbor(nbor_list_type &nlt);
    ~rating_reader() noexcept {}
};

#endif /* DB_CTRL_H */
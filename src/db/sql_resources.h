#ifndef SQL_H
#define SQL_H

namespace sql
{
    extern const char* create_table_users;
    extern const char* create_table_user_deals;
    extern const char* insert_user;
    extern const char* insert_deal;
    extern const char* update_user;
    extern const char* select_rates;
}


#endif /* SQL_H */


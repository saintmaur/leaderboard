#include "sql_resources.h"
namespace sql
{
    const char* create_table_users{"create table if not exists users\n"
            "(\n"
            "   id integer not null primary key,\n"
            "   name varchar(200) not null,\n"
            "   conn_state smallint not null default 0 check (conn_state in (0,1))\n"
            ");"};
    const char* create_table_user_deals{"create table if not exists user_deals\n"
            "(\n"
            "   user_id integer not null,\n"
            "   amount numeric not null default 0,\n"
            "   time timestamp not null,\n"
            "   win_flag smallint not null default 0 check (win_flag in (0,1))\n"
            ");"};
    const char* insert_user{"insert into users(id, name) "
                            "values (:id<int,in>, :name<char[201],in>)"};
    const char* insert_deal{"insert into user_deals(user_id, amount, time, win_flag) "
            "values (:id<int,in>, :amount<double,in>, "
            ":time<timestamp,in>, :flag<int,in>)"};
    const char* update_user{"update users set name = coalesce(:name<char[201],in>,name),"
            " conn_state = coalesce(:st<int,in>,conn_state) where id = :id<int,in>"};
    const char* select_rates = "select distinct user_id, sum(amount) full_sum, id, conn_state from user_deals "
    "left join users on user_id = id "
    "where time between date_trunc('week', current_date) "
    "and date_trunc('week', current_date) + interval '1 week' - interval '1 second' "
    "group by user_id, id, win_flag "
    "order by full_sum desc";
}
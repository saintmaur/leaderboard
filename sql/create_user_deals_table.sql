create table user_deals
(
    user_id integer not null, 
    amount numeric not null default 0, 
    time timestamp not null,
    win_flag smallint not null default 0 check (win_flag in (0,1))
);
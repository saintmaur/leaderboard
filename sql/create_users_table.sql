create table users
(
    id integer not null primary key,
    name varchar(200) not null,
    conn_state smallint not null default 0 check (conn_state in (0,1))
);
#include "gtest/gtest.h"
#include "../src/db/db_ctrl.h"

TEST(lcol_suite, can_check_capacity_and_act)
{
    unsigned max = 3;
    int check_val = 555;
    limited_collection<int> lc(max, [&check_val](const limited_collection<int>::collection_type& c){ check_val++; });
    for(int i = 0; i < 5; i++)
    {
        lc.push(i);
    }
    ASSERT_TRUE(lc.size() == max);
    ASSERT_EQ(check_val, 556);
}

TEST(lcol_suite, can_flush)
{
    unsigned max = 3;
    int check_val = 555;
    limited_collection<int> lc(max, [&check_val](const limited_collection<int>::collection_type& c){ check_val++; });
    for(int i = 0; i < 1; i++)
    {
        lc.push(i);
    }
    ASSERT_FALSE(lc.size() == max);
    ASSERT_EQ(check_val, 555);
    lc.flush();
    ASSERT_EQ(check_val, 556);
}
#include "gtest/gtest.h"
#include "../src/dispatcher.h"
#include "../src/db/db_ctrl.h"
#include <sstream>
class msg_disp_suite : public ::testing::Test
{
protected:
    const unsigned      to_ms = 250;
    const unsigned      bulk_size = 100;
    user_inserter       user_reg_ctrl;
    user_updater        user_upd_ctrl;
    user_deal_inserter  user_deal_ctrl;
    dispatcher          dspt;
public:
    msg_disp_suite() 
    : user_reg_ctrl("user_reg", "DRIVER=/usr/lib/x86_64-linux-gnu/odbc/psqlodbca.so;"
                                        "DATABASE=test_db;"
                                        "UID=pg_user_1;"
                                        "PWD=1111;"
                                        "SERVER=localhost;"
                                        "PORT=5432;", 
                    std::chrono::seconds(1), bulk_size)
    , user_upd_ctrl("user_upd", "DRIVER=/usr/lib/x86_64-linux-gnu/odbc/psqlodbca.so;"
                                        "DATABASE=test_db;"
                                        "UID=pg_user_1;"
                                        "PWD=1111;"
                                        "SERVER=localhost;"
                                        "PORT=5432;",
                    std::chrono::seconds(1), bulk_size)
    , user_deal_ctrl("user_deal", "DRIVER=/usr/lib/x86_64-linux-gnu/odbc/psqlodbca.so;"
                                        "DATABASE=test_db;"
                                        "UID=pg_user_1;"
                                        "PWD=1111;"
                                        "SERVER=localhost;"
                                        "PORT=5432;",
                    std::chrono::seconds(1), bulk_size)
    , dspt(bulk_size, to_ms, user_reg_ctrl, user_upd_ctrl, user_deal_ctrl)
    {
        dspt.run();
    }
    
    void TearDown()
    {
        dspt.stop();
        if(dspt.is_started())
        {
            dspt.join();
        }
        user_reg_ctrl.stop();
        user_upd_ctrl.stop();
        user_deal_ctrl.stop();
    }
};

TEST_F(msg_disp_suite, can_pass_msgs)
{
    unsigned max = 100;
    bool res = false;
    for(int i = 0; i < max; i++)
    {
        std::stringstream strm;
        strm << "user-" << i;
        res = dspt.get_user_reg_queue()
                                .push(std::move(msg::user_reg{i, strm.str().c_str()}));
        ASSERT_EQ(true, res) << "Failed to push the message #" << i;
    }
    ASSERT_TRUE(res);
}

/*//            

    //"pg_user_1/1111@test_db"*/
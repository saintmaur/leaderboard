#include "gtest/gtest.h"
#include "../src/cloptions.h"

TEST(cloptions_suite, can_parse_options)
{
    char* opts_str[] = {(char*)"leaderboard_test",
                        (char*)"--amqp_host", 
                        (char*)"localhost", 
                        (char*)"--amqp_port", 
                        (char*)"5672",
                        (char*)"--amqp_exchange", 
                        (char*)"test_exchange", 
                        (char*)"--amqp_queue", 
                        (char*)"test_queue",
                        (char*)"--amqp_rkey", 
                        (char*)"test_rkey", 
                        (char*)"--db_connect", 
                        (char*)"DRIVER=/usr/lib/i386-linux-gnu"
                                "/odbc/psqlodbca.so;DATABASE=test_db;UID=pg_user_1;PWD=1111;"
                                "SERVER=localhost;PORT=5432;"};
    config_type config;

    //ASSERT_TRUE(get_config(7, opts_str, config));
    get_config(13, opts_str, config);

    ASSERT_TRUE(config.amqp_host.compare("localhost") == 0);
    ASSERT_TRUE(config.amqp_port == 5672);
    ASSERT_TRUE(config.amqp_exchange.compare("test_exchange") == 0);
    ASSERT_EQ(0, config.amqp_queue.compare("test_queue")) << "Got queue: " << config.amqp_queue;
    ASSERT_EQ(0, config.amqp_rkey.compare("test_rkey")) << "Got key: " << config.amqp_rkey;
    ASSERT_EQ(0, config.db_connect.compare("DRIVER=/usr/lib/i386-linux-gnu"
                                "/odbc/psqlodbca.so;DATABASE=test_db;UID=pg_user_1;PWD=1111;"
                                "SERVER=localhost;PORT=5432;")) << "Got connect: " << config.db_connect;
}
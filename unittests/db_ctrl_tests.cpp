#include "gtest/gtest.h"
#include "../src/dispatcher.h"
#include "../src/interface.h"


class db_ctrl_suite : public ::testing::Test
{
protected:
    const unsigned      bulk_size = 10;
public:
    void create_tables(otl_connect& conn)
    {
        try
        {
            otl_cursor::direct_exec(conn, sql::create_table_users);
            otl_cursor::direct_exec(conn, sql::create_table_user_deals);
        }
        catch(otl_exception& e)
        {
            FAIL() << e.msg << std::endl;
        }
    }
    void cleanup(otl_connect& conn)
    {
        try
        {
            std::stringstream delete_sql_strm;
            delete_sql_strm << "delete from users where id in (";
            for(int i = 0; i < bulk_size; i++)
            {
                delete_sql_strm << i << ",";
            }
            delete_sql_strm.seekp(-1, delete_sql_strm.cur);
            delete_sql_strm << ")";
            otl_cursor::direct_exec(conn, delete_sql_strm.str().c_str());
            
            delete_sql_strm.str("");
            delete_sql_strm << "delete from user_deals where user_id in (";
            for(int i = 0; i < bulk_size; i++)
            {
                delete_sql_strm << i << ",";
            }
            delete_sql_strm.seekp(-1, delete_sql_strm.cur);
            delete_sql_strm << ")";
            otl_cursor::direct_exec(conn, delete_sql_strm.str().c_str());
        }
        catch(otl_exception& e)
        {
            FAIL() << e.msg << std::endl;
        }
    }
};

TEST_F(db_ctrl_suite, can_convert_tm_to_otl_datetime)
{
    const char* format = "%Y.%m.%d %H:%M:%S";
    std::string date_str = "2017.05.01 01:01:01";
    bool res = false;
    auto time = msg::get_time_from_string(format, date_str, res);
    
    auto otl_dt = get_otl_dt(time);
    
    res = otl_dt.second == 1 && otl_dt.minute == 1 && otl_dt.hour == 1
                && otl_dt.day == 1 && otl_dt.month == 5 && otl_dt.year == 2017;
    ASSERT_EQ(true, res) << "Failed to convert time_t to otl_datetime";
}

TEST_F(db_ctrl_suite, can_insert_user)
{
    user_inserter user_reg_ctrl("user_reg", "DRIVER=/usr/lib/x86_64-linux-gnu/odbc/psqlodbca.so;" //DRIVER=/usr/lib/x86_64-linux-gnu/odbc/psqlodbca.so;DATABASE=test_db;UID=postgres;PWD=1111;SERVER=localhost;PORT=5432;
                                        "DATABASE=test_db;"
                                        "UID=postgres;"
                                        "PWD=1111;"
                                        "SERVER=localhost;"
                                        "PORT=5432;", 
                    std::chrono::seconds(1), bulk_size);
    if(user_reg_ctrl.run())
    {
        bool res = user_reg_ctrl.is_connected();
        ASSERT_TRUE(res);

        create_tables(user_reg_ctrl.get_connect());
        
        std::vector<msg::user_reg> v;
        for (int i = 0; i < bulk_size; i++)
        {
            v.push_back(std::move(msg::user_reg{i, "name"}));
        }
        user_reg_ctrl.get_queue().push(std::move(v));

        sleep(1);

        std::string check_sql_str("select count(*) from users");

        auto &conn = user_reg_ctrl.get_connect();
        try
        {
            otl_stream strm(1, check_sql_str.c_str(), conn);
            int cnt = -1;
            strm >> cnt;
            ASSERT_EQ(bulk_size, cnt)
                    << "The count of inserted records mismatch: "
                    << "(actual/expected) " << cnt << "/" << bulk_size;
        }
        catch(otl_exception& e)
        {
            FAIL() << e.msg;
        }
        cleanup(user_reg_ctrl.get_connect());
        user_reg_ctrl.stop();
        user_reg_ctrl.join();
    }
    else
    {
        FAIL() << "Could not start user_reg_ctrl";
    }
}
TEST_F(db_ctrl_suite, can_update_user)
{
    user_updater user_upd_ctrl("user_upd", "DRIVER=/usr/lib/x86_64-linux-gnu/odbc/psqlodbca.so;"
                                        "DATABASE=test_db;"
                                        "UID=postgres;"
                                        "PWD=1111;"
                                        "SERVER=localhost;"
                                        "PORT=5432;",
                    std::chrono::seconds(1), bulk_size);
    if(user_upd_ctrl.run())
    {
        bool res = user_upd_ctrl.is_connected();
        ASSERT_TRUE(res);

        std::vector<msg::user_upd> v;
        for (int i = 0; i < bulk_size; i++)
        {
            v.push_back(std::move(msg::user_upd{i, "", 1}));
        }
        user_upd_ctrl.get_queue().push(std::move(v));

        sleep(1);

        std::string check_sql_str("select count(*) from users where conn_state = 0");

        auto &conn = user_upd_ctrl.get_connect();
        try
        {
            otl_stream strm(1, check_sql_str.c_str(), conn);
            int cnt = -1;
            strm >> cnt;
            ASSERT_EQ(0, cnt)
                    << "The count of updated records mismatch: "
                    << "(actual/expected) " << cnt << "/0";
        }
        catch(otl_exception& e)
        {
            FAIL() << e.msg;
        }
        cleanup(user_upd_ctrl.get_connect());
        user_upd_ctrl.stop();
        user_upd_ctrl.join();
    }
    else
    {
        FAIL() << "Could not start user_upd_ctrl";
    }
}
TEST_F(db_ctrl_suite, can_insert_deal)
{
    user_deal_inserter user_deal_ctrl("user_deal", "DRIVER=/usr/lib/x86_64-linux-gnu/odbc/psqlodbca.so;"
                                        "DATABASE=test_db;"
                                        "UID=postgres;"
                                        "PWD=1111;"
                                        "SERVER=localhost;"
                                        "PORT=5432;",
                    std::chrono::seconds(1), bulk_size);
    if(user_deal_ctrl.run())
    {
        bool res = user_deal_ctrl.is_connected();
        ASSERT_TRUE(res);

        std::vector<msg::user_deal> v;
        time_t t;
        time(&t);
        auto tm = *localtime(&t);
        for (int i = 0; i < bulk_size; i++)
        {
            v.push_back(std::move(msg::user_deal{i, 10.0, tm, 0}));
        }
        user_deal_ctrl.get_queue().push(std::move(v));

        sleep(1);

        std::string check_sql_str("select count(*) from user_deals");

        auto &conn = user_deal_ctrl.get_connect();
        try
        {
            otl_stream strm(1, check_sql_str.c_str(), conn);
            int cnt = -1;
            strm >> cnt;
            ASSERT_EQ(bulk_size, cnt)
                    << "The count of updated records mismatch: "
                    << "(actual/expected) " << cnt << "/" << bulk_size;
        }
        catch(otl_exception& e)
        {
            FAIL() << e.msg;
        }
        cleanup(user_deal_ctrl.get_connect());
        user_deal_ctrl.stop();
        user_deal_ctrl.join();
    }
    else
    {
        FAIL() << "Could not start user_deal_ctrl";
    }
}

TEST_F(db_ctrl_suite, can_make_joint_select)
{
    int user1_id = 1;
    int user2_id = 2;
    // insert users
    user_inserter user_reg_ctrl("user_reg", "DRIVER=/usr/lib/x86_64-linux-gnu/odbc/psqlodbca.so;"
                                        "DATABASE=test_db;"
                                        "UID=postgres;"
                                        "PWD=1111;"
                                        "SERVER=localhost;"
                                        "PORT=5432;");
    std::vector<msg::user_reg> v1;
    v1.push_back(std::move(msg::user_reg{user1_id, "name"}));
    v1.push_back(std::move(msg::user_reg{user2_id, "name"}));

    user_reg_ctrl.connect();
    user_reg_ctrl.process_bulk(v1);
    // insert deals
    user_deal_inserter user_deal_ctrl("user_deal", "DRIVER=/usr/lib/x86_64-linux-gnu/odbc/psqlodbca.so;"
                                        "DATABASE=test_db;"
                                        "UID=postgres;"
                                        "PWD=1111;"
                                        "SERVER=localhost;"
                                        "PORT=5432;");
    
    std::vector<msg::user_deal> v2;
    time_t t;
    time(&t);
    auto tm = *localtime(&t);
    for (int i = 0; i < bulk_size; i++)
    {
        v2.push_back(std::move(msg::user_deal{user1_id, 10.0, tm, i%2}));
    }
    v2.push_back(std::move(msg::user_deal{user2_id, 10.0, tm, 0}));
    
    user_deal_ctrl.connect();
    user_deal_ctrl.process_bulk(v2);
    
    user_updater        user_upd_ctrl("user_upd", "DRIVER=/usr/lib/x86_64-linux-gnu/odbc/psqlodbca.so;"
                                        "DATABASE=test_db;"
                                        "UID=postgres;"
                                        "PWD=1111;"
                                        "SERVER=localhost;"
                                        "PORT=5432;");
    
    std::vector<msg::user_upd> v3;
    v3.push_back(std::move(msg::user_upd{user1_id, "", 1}));
    user_upd_ctrl.connect();
    user_upd_ctrl.process_bulk(v3);

    dispatcher d(bulk_size, 500, user_reg_ctrl, user_upd_ctrl, user_deal_ctrl);

    amqp_writer awr(d, "test_exchange", "test_queue", "test_rkey");
    
    rating_reader reader("rating_data", "DRIVER=/usr/lib/x86_64-linux-gnu/odbc/psqlodbca.so;"
                                        "DATABASE=test_db;"
                                        "UID=postgres;"
                                        "PWD=1111;"
                                        "SERVER=localhost;"
                                        "PORT=5432;",
                        std::chrono::seconds(60), awr.get_queue());
    rating_reader::rate_list_type rl;
    reader.connect();
    reader.read_rating(rl);
    ASSERT_TRUE(rl.size() > 0);
    const auto & rd = *rl.begin();
    ASSERT_TRUE(rd.rating_list.size() > 0);
    ASSERT_TRUE(rd.rating_list.begin()->amount == 50);
    
    ASSERT_TRUE(rd.neighbors.size() > 0);
    ASSERT_TRUE(rd.neighbors.begin()->user_id == user2_id);
    // deinit
    reader.stop();
    user_reg_ctrl.stop();
    user_upd_ctrl.stop();
    user_deal_ctrl.stop();
    d.stop();
    awr.stop();
    cleanup(user_reg_ctrl.get_connect());
}
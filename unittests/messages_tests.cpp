#include "gtest/gtest.h"
#include "../src/dispatcher.h"
#include <sstream>

TEST(messages_suite, can_parse_date)
{
    const char* format = "%Y.%m.%d %H:%M:%S";
    std::string date_str = "2017.05.01 00:00:01";
    bool res = false;
    auto time = msg::get_time_from_string(format, date_str, res);
    ASSERT_EQ(true, res) << "Failed to parse the date: " << date_str
                        << " by format: " << format << std::endl;
    std::stringstream out;
    out << std::put_time(&time, format);
    
    ASSERT_EQ(117, time.tm_year) << "Failed to convert date(year): " << time.tm_year;
    ASSERT_EQ(0, date_str.compare(out.str()))
            << "Failed to convert string: "
            << out.str()
            << std::endl;
}

TEST(messages_suite, can_parse_user_reg)
{
    try
    {
        std::string msg = "user_registered(789,name)";
        std::string msg_id = "user_registered";

        auto m = msg::make_user_reg_msg(msg, msg_id);

        ASSERT_EQ(789, m.id);
        ASSERT_EQ("name", m.name);
    }
    catch(std::exception& e)
    {
        FAIL() << e.what();
    }
}

TEST(messages_suite, can_parse_user_rename)
{
    try
    {
        std::string msg = "user_renamed(789,new name)";
        std::string msg_id = "user_renamed";

        auto m = msg::make_user_upd_msg(msg, msg_id);

        ASSERT_EQ(789, m.id);
        ASSERT_EQ("new name", m.name);
        ASSERT_EQ(0, m.conn_state);
    }
    catch(std::exception& e)
    {
        FAIL() << e.what();
    }
}

TEST(messages_suite, can_parse_user_connect)
{
    try
    {
        std::string msg = "user_connected(789)";
        std::string msg_id = "user_connected";

        auto m = msg::make_user_upd_msg(msg, msg_id);

        ASSERT_EQ(789, m.id);
        ASSERT_TRUE(m.name.empty());
        ASSERT_EQ(1, m.conn_state);
    }
    catch(std::exception& e)
    {
        FAIL() << e.what();
    }
}

TEST(messages_suite, can_parse_user_disconnect)
{
    try
    {
        std::string msg = "user_disconnected(789)";
        std::string msg_id = "user_disconnected";

        auto m = msg::make_user_upd_msg(msg, msg_id);

        ASSERT_EQ(789, m.id);
        ASSERT_TRUE(m.name.empty());
        ASSERT_EQ(0, m.conn_state);
    }
    catch(std::exception& e)
    {
        FAIL() << e.what();
    }
}

TEST(messages_suite, can_parse_user_deal)
{
    try
    {
        std::string msg = "user_deal(789, 2017.05.01 01:07:01, 101)";
        std::string msg_id = "user_deal";

        auto m = msg::make_user_deal_msg(msg, msg_id);

        ASSERT_EQ(789, m.user_id);
        ASSERT_EQ(7, m.time.tm_min);
        ASSERT_EQ(101, m.amount);
        ASSERT_EQ(0, m.won);
        
        msg = "user_deal(789, 2017.05.01 01:07:01, 101.05)";
        m = msg::make_user_deal_msg(msg, msg_id);
        ASSERT_EQ(101.05, m.amount);
    }
    catch(std::exception& e)
    {
        FAIL() << e.what();
    }
}

TEST(messages_suite, can_parse_user_deal_won)
{
    try
    {
        std::string msg = "user_deal_won(789, 2017.05.01 01:07:01, 101)";
        std::string msg_id = "user_deal_won";

        auto m = msg::make_user_deal_msg(msg, msg_id);

        ASSERT_EQ(789, m.user_id);
        ASSERT_EQ(7, m.time.tm_min);
        ASSERT_EQ(101, m.amount);
        ASSERT_EQ(1, m.won);
    }
    catch(std::exception& e)
    {
        FAIL() << e.what();
    }
}
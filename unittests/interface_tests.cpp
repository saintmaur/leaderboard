#include "gtest/gtest.h"
#include "../src/interface.h"
#include "../src/db/db_ctrl.h"

class interface_suite : public ::testing::Test
{
protected:
    user_inserter       i;
    user_updater        u;
    user_deal_inserter  di;
    dispatcher          d;
    amqp_reader         reader;
    amqp_writer         writer;
public:
    interface_suite() 
    : i("","")
    , u("","")
    , di("","")
    , d(0, 0, i, u, di)
    , reader(d, "test_exchange", "test_rkey", "test_queue", 
                "localhost", 5672, 1)
    , writer(d, "test_exchange", "test_rkey", "test_queue", 
                "localhost", 5672, 2){}
    void SetUp()
    {
        if(!reader.init() || !writer.init())
        {
            throw std::runtime_error("Could not initialize AMQP interface");
        }
    }
    void TearDown()
    {
        reader.deinit();
        reader.stop();
        
        writer.deinit();
        writer.stop();
        d.stop();
        i.stop();
        u.stop();
        di.stop();
    }
};

TEST_F(interface_suite, can_connect)
{
    ASSERT_TRUE(reader.connected());
}
TEST_F(interface_suite, can_reinit)
{
    reader.deinit();
    ASSERT_FALSE(reader.connected());
    reader.init();
    ASSERT_TRUE(reader.connected());
}

TEST_F(interface_suite, can_declare_exchange)
{
    ASSERT_TRUE(reader.declare_exchange());
}

TEST_F(interface_suite, can_declare_queue)
{
    ASSERT_EQ(true, reader.declare_queue("test_queue"))
             << "Got an error on queue declaring: " << reader.get_error_message();
}

TEST_F(interface_suite, can_bind_queue)
{
    auto res = reader.bind_queue("test_queue", "test_exchange", "test_rkey");
    ASSERT_EQ(true, res) 
            << "Got an error on queue binding: " << reader.get_error_message();
}

TEST_F(interface_suite, can_consume)
{
    ASSERT_TRUE(reader.consume());
}

TEST_F(interface_suite, can_purge_queue)
{
    ASSERT_EQ(true, writer.queue_purge("test_queue")) << reader.get_error_message();
}

TEST_F(interface_suite, can_post_message)
{
    ASSERT_TRUE(writer.post_message("reader.post_message(1)"));
    writer.queue_purge("test_queue");
}
TEST_F(interface_suite, can_recieve_message)
{
    ASSERT_TRUE(writer.post_message("reader.post_message(2)"));
    std::string msg;
    ASSERT_EQ(true, reader.consume()) << reader.get_error_message();
    uint64_t dtag{757};
    ASSERT_EQ(true, reader.listen(msg, dtag)) << reader.get_error_message();
    ASSERT_NE(757, dtag);
    writer.queue_purge("test_queue");
}

TEST_F(interface_suite, can_ack)
{
    ASSERT_TRUE(writer.post_message("reader.post_message(3)"));
    std::string msg;
    uint64_t dtag;
    ASSERT_EQ(true, reader.consume()) << reader.get_error_message();
    ASSERT_EQ(true, reader.listen(msg, dtag)) << reader.get_error_message();
    ASSERT_EQ(true, reader.ack(dtag));
    ASSERT_FALSE(reader.listen(msg, dtag));
}
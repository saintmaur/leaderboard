cmake_minimum_required (VERSION 2.8)

project (control_simulator)

set (APP_NAME leaderboard)
set (APP_NAME_TEST ${APP_NAME}_test)
#set (GTEST_ROOT /home/seymour/src/googletest/googletest)
set(CMAKE_CXX_COMPILER $ENV{CXX})

message(STATUS "-------------------------------------------------------------")
message(STATUS "C Compiler: ${CMAKE_C_COMPILER}")
message(STATUS "CXX Compiler: ${CMAKE_CXX_COMPILER}")
message(STATUS "CXX Compiler (env): $ENV{CXX}")
message(STATUS "-------------------------------------------------------------")

set(DEBUG_CXX_FLAGS "-O0 -g3 -fno-inline -march=core2 -pipe")

set(CMAKE_CXX_FLAGS "-std=c++11 -pthread " ${DEBUG_CXX_FLAGS})

set(RMQ_NAME "rabbitmq-c")
set(RMQ_LIB ${RMQ_NAME}_library)

include(ExternalProject)
set(RMQ_INSTALL_DIR ${CMAKE_BINARY_DIR}/${RMQ_NAME})
ExternalProject_Add(${RMQ_NAME}_project
        SOURCE_DIR ${CMAKE_SOURCE_DIR}/${RMQ_NAME}
        CMAKE_ARGS
            -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
            -DBUILD_SHARED_LIBS=OFF
            -DBUILD_TOOLS=OFF
            -DBUILD_EXAMPLES=OFF
            -DBUILD_TESTS=OFF
            -DCMAKE_INSTALL_LIBDIR=<INSTALL_DIR>/lib
        INSTALL_DIR ${RMQ_INSTALL_DIR})
set(RMQ_INCLUDE_DIR ${RMQ_INSTALL_DIR}/include)
add_library(${RMQ_NAME}_library STATIC IMPORTED)
set_property(TARGET ${RMQ_LIB} PROPERTY IMPORTED_LOCATION
        "${RMQ_INSTALL_DIR}/lib/librabbitmq.a")

set(GTEST_NAME "gtest")
set(GTEST_LIB ${GTEST_NAME}_library)

include(ExternalProject)
set(GTEST_INSTALL_DIR ${CMAKE_BINARY_DIR}/${GTEST_NAME})
ExternalProject_Add(${GTEST_NAME}_project
        SOURCE_DIR ${CMAKE_SOURCE_DIR}/${GTEST_NAME}
        CMAKE_ARGS
            -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
            -DCMAKE_BUILD_TYPE=Debug
        INSTALL_DIR ${GTEST_INSTALL_DIR})
set(GTEST_INCLUDE_DIR ${GTEST_INSTALL_DIR}/include)
add_library(${GTEST_NAME}_library STATIC IMPORTED)
set_property(TARGET ${GTEST_LIB} PROPERTY IMPORTED_LOCATION
        "${GTEST_INSTALL_DIR}/lib/libgtest.a")

set (HEADERS    ./src/message.h
                ./src/active_object.h
                ./src/dispatcher.h
                ./src/interface.h
                ./src/event_queue.h
                ./src/bulks.h
                ./src/cloptions.h
                )

set (SOURCES    ./src/message.cpp
                ./src/dispatcher.cpp
                ./src/interface.cpp
)
include(${CMAKE_CURRENT_LIST_DIR}/src/db/CMakeLists.txt)

include_directories(${RMQ_INCLUDE_DIR})
include_directories("/opt/include")
include_directories(AMQP-CPP AMQP-CPP/include)
link_directories("/opt/lib")
add_executable(${APP_NAME} ${HEADERS} ${SOURCES} ./src/main.cpp)

target_link_libraries(${APP_NAME} -lodbc ${RMQ_LIB})
#####################
### Compile tests ###
#####################
set (TEST_SOURCES   ${SOURCES} 
                    ./unittests/main.cpp
                    ./unittests/messages_tests.cpp
                    ./unittests/msg_dispatch_tests.cpp
                    ./unittests/db_ctrl_tests.cpp
                    ./unittests/interface_tests.cpp
                    ./unittests/limited_collection_tests.cpp
                    ./unittests/cloptions_tests.cpp)

#find_package(GTest REQUIRED)
#include_directories(${GTest_INCLUDE_DIRS})
include_directories(${GTEST_INCLUDE_DIR})
set(CMAKE_CXX_FLAGS "-std=c++11 -pthread -fprofile-arcs -ftest-coverage")
add_executable(${APP_NAME_TEST} ${HEADERS} ${SOURCES} ${TEST_SOURCES})
target_link_libraries(${APP_NAME_TEST} 
                    #${GTEST_LIBRARIES} 
                    ${GTEST_LIB}
                    -lgcov -lodbc ${RMQ_LIB})
enable_testing()
add_test(${APP_NAME_TEST} ${APP_NAME_TEST})
##############################################
### Run tests and check the code coverage ###
##############################################
set(LCOV_TRACE_FILE tracefile_total.info)
if (DEFINED ENV{LCOV_TRACE_FILE})
    set(LCOV_TRACE_FILE $ENV{LCOV_TRACE_FILE})
endif()

set(JUNIT_REPORT_FILE gtest_junit_report.xml)
add_custom_target(coverage
		lcov --directory ${CMAKE_BINARY_DIR}
		--capture --initial --output-file tracefile_base.info
	COMMAND ${CMAKE_BINARY_DIR}/${APP_NAME_TEST}
		--gtest_output=xml:${JUNIT_REPORT_FILE}
	COMMAND lcov --directory ${CMAKE_BINARY_DIR}
		--capture --output-file tracefile.info
	COMMAND lcov -a tracefile_base.info -a tracefile.info
		-o ${LCOV_TRACE_FILE}
	COMMAND genhtml ${LCOV_TRACE_FILE}
	)